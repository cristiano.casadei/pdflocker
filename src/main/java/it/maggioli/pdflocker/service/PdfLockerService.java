package it.maggioli.pdflocker.service;

import it.maggioli.pdflocker.exception.PdfLockerException;
import it.maggioli.pdflocker.request.lockRequest.LockOptions;
import it.maggioli.pdflocker.request.lockRequest.WatermarkOptions;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.util.Matrix;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Servizio di blocco PDF
 *
 * @author CCasadei
 */
@Service
public class PdfLockerService {

    /**
     * Risoluzione di default in caso di renderizzazione del PDF
     */
    public static final float DEFAULT_DPI = 300.0f;

    /**
     * Font utilizzato per il watermark
     */
    private static final PDFont FONT = PDType1Font.HELVETICA_BOLD;

    /**
     * Dimensione font utilizzato per il watermark
     */
    private static final float FONT_SIZE = 10.0f;

    /**
     * Margine da utilizzare per l'applicazione del watermark
     */
    private static final float MARGIN = 5.0f;

    /**
     * Applica l'eventuale watermark, renderizza in grafica se richiesto e blocca il PDF applicando i permessi indicati
     * @param file File PDF da elaborare
     * @param watermarkOptions Struttura dati delle opzioni del watermark
     * @param lockOptions Struttura dati delle opzioni di blocco e rendering
     * @return File PDF elaborato
     */
    public ByteArrayOutputStream lock(MultipartFile file,
                                      WatermarkOptions watermarkOptions,
                                      LockOptions lockOptions) {
        try {
            // imposto le dimensioni del buffer di parsing PDF per evitare problemi con PDF di grandi dimensioni
            System.setProperty("org.apache.pdfbox.baseParser.pushBackSize", "2024768");

            // carico il PDF in un documento
            PDDocument document = PDDocument.load(file.getInputStream());

            // applico l'eventuale watermark alle pagine del PDF
            drawWatermark(document, watermarkOptions);

            // se necessario, renderizzo in immagini il PDF in modo da ottenerne una versione grafica
            // dalla quale sarà impossile estrapolare testo
            if ((lockOptions.getRenderImage() != null) && (lockOptions.getRenderImage())) {
                document = renderImage(document, lockOptions);
            }

            // applico i permessi indicati nelle opzioni di blocco
            applyPermissions(document, lockOptions);

            // esporto il documento PDF su uno stream in memoria
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            document.save(outputStream);
            document.close();
            outputStream.flush();

            // decommentare in caso di debug per memorizzare il file PDF elaborato direttamente su file system
//            FileOutputStream debugOutputStream = new FileOutputStream("/tmp/PROVA.pdf");
//            debugOutputStream.write(outputStream.toByteArray());
//            debugOutputStream.close();

            // ritorno lo stream del PDF elaborato
            return outputStream;
        } catch (Exception ex) {
            throw new PdfLockerException("Non posso elaborare il file.", ex);
        }
    }

    /**
     * Disegno l'eventuale watermark sulle pagine del PDF
     * @param document Documento PDF
     * @param watermarkOptions Struttura dati con le opzioni del watermark
     * @throws IOException
     */
    private void drawWatermark(PDDocument document, WatermarkOptions watermarkOptions) throws IOException {
        // se non ci sono opzioni di watermark, esco senza toccare il documento PDF
        if (watermarkOptions != null) {
            // calcolo le dimensioni del watermark, in modo da poterlo posizionare correttamente
            float stringWidth = FONT.getStringWidth(watermarkOptions.getText()) / 1000 * FONT_SIZE;

            // ripeto per ogni pagina del documento PDF
            for (PDPage page : document.getPages()) {
                // creo un content stream per la pagina corrente
                PDPageContentStream contentStream = new PDPageContentStream(
                        document,
                        page,
                        PDPageContentStream.AppendMode.APPEND,
                        true,
                        true);
                // inizio a scrivere
                contentStream.beginText();
                // imposto il font da utilizzare
                contentStream.setFont(FONT, FONT_SIZE);

                // posiziono il testo in base all'opzione di posizionamento
                switch (watermarkOptions.getPosition().toUpperCase()) {
                    // posizionamento in alto alla pagina, centrato in larghezza
                    case "TOP":
                        contentStream.transform(Matrix.getRotateInstance(
                                Math.toRadians(0),
                                page.getMediaBox().getLowerLeftX() + ((page.getMediaBox().getWidth() - stringWidth) / 2),
                                page.getMediaBox().getUpperRightY() - MARGIN - FONT_SIZE
                        ));
                        break;

                    // posizionamento in basso alla pagina, centrato in larghezza
                    case "BOTTOM":
                        contentStream.transform(Matrix.getRotateInstance(
                                Math.toRadians(0),
                                page.getMediaBox().getLowerLeftX() + ((page.getMediaBox().getWidth() - stringWidth) / 2),
                                page.getMediaBox().getLowerLeftY() + MARGIN
                        ));
                        break;

                    // posizionamento sul bordo sinistro della pagina, centrato in altezza e ruotato a 90°
                    case "LEFT":
                        contentStream.transform(Matrix.getRotateInstance(
                                Math.toRadians(90),
                                page.getMediaBox().getLowerLeftX() + MARGIN,
                                page.getMediaBox().getLowerLeftY() + ((page.getMediaBox().getHeight() - stringWidth) / 2)
                        ));
                        break;

                    // posizionamento sul bordo destro della pagina, centrato in altezza e ruotato a 270°
                    case "RIGHT":
                        contentStream.transform(Matrix.getRotateInstance(
                                Math.toRadians(270),
                                page.getMediaBox().getUpperRightX() - MARGIN - FONT_SIZE,
                                page.getMediaBox().getLowerLeftY() + ((page.getMediaBox().getHeight() - stringWidth) / 2) + stringWidth
                        ));
                        break;

                    // posizionamento non riconosciuto, quindi sollevo una eccezione
                    default:
                        throw new PdfLockerException("La posizione del watermark non è valida" +
                                (watermarkOptions.getPosition() != null ? " ('" + watermarkOptions.getPosition() + "')" : ""));
                }

                // scrivo il testo del watermark usando le opzioni di rotazione e posizionamento impostati in precedenza
                contentStream.showText(watermarkOptions.getText());

                // finisco le modifiche testuali e chiudo il content stream
                contentStream.endText();
                contentStream.close();
            }
        }
    }

    /**
     * Applica i permessi di blocco al file PDF
     * @param document Documento PDF
     * @param lockOptions Struttura dati delle opzioni di blocco
     * @throws IOException
     */
    private void applyPermissions(PDDocument document, LockOptions lockOptions) throws IOException {
        // creo un nuovo oggetto con cui impostare i permessi di blocco
        AccessPermission accessPermission = new AccessPermission();

        // in base alle opzioni di blocco, abilito/disabilito i vari permessi
        accessPermission.setCanExtractForAccessibility((lockOptions.getCanScreenReader() != null) && (lockOptions.getCanScreenReader()));
        accessPermission.setCanExtractContent((lockOptions.getCanCopy() != null) && (lockOptions.getCanCopy()));
        accessPermission.setCanFillInForm((lockOptions.getCanFillIn() != null) && (lockOptions.getCanFillIn()));
        accessPermission.setCanModify((lockOptions.getCanModify() != null) && (lockOptions.getCanModify()));
        accessPermission.setCanModifyAnnotations((lockOptions.getCanModify() != null) && (lockOptions.getCanModify()));
        accessPermission.setCanPrint((lockOptions.getCanPrint() != null) && (lockOptions.getCanPrint()));
        accessPermission.setCanPrintDegraded((lockOptions.getCanPrint() != null) && (lockOptions.getCanPrint()));
        accessPermission.setCanAssembleDocument((lockOptions.getCanMerge() != null) && (lockOptions.getCanMerge()));

        // imposto il PDF in sola lettura
        accessPermission.setReadOnly();

        // creo una policy di protezione usando i permessi impostati in precenza, l'eventuale password utente e la password owner
        // NOTA: se la password owner non è impostata, viene utilizzata una password casuale
        StandardProtectionPolicy stdProtectionPolicy = new StandardProtectionPolicy(
                StringUtils.isEmpty(lockOptions.getOwnerPassword()) ? UUID.randomUUID().toString() : lockOptions.getOwnerPassword(),
                lockOptions.getUserPassword(),
                accessPermission
        );

        // indico che per la criptazione è preferibile utilizzare AES, se disponibile
        stdProtectionPolicy.setPreferAES(true);

        // applica il blocco al documento PDF
        document.protect(stdProtectionPolicy);
    }

    /**
     * Renderizza in immagini il PDF, ricostruendo un nuovo PDF costituito solo di pagine grafiche da cui non è possibile estrarre testo
     * @param document Documento PDF
     * @param lockOptions Struttura dati con le opzioni di rendering
     * @return Nuovo documento PDF renderizzato in immagini
     * @throws IOException
     */
    private PDDocument renderImage(PDDocument document, LockOptions lockOptions) throws IOException {
        // creo il nuovo documento PDF sul quale applicare le pagine del PDF originale renderizzate
        PDDocument renderedDocument = new PDDocument();

        // creo un renderer
        PDFRenderer renderer = new PDFRenderer(document);

        // ripeto per ogni pagina del documento PDf originale
        for (int pageIdx = 0; pageIdx < document.getNumberOfPages(); pageIdx++) {
            // creo una immagine dalla pagina corrente usando i parametri di renderizzazione
            // NOTA: se la risoluzione di rendering non è specificata, viene utilizzato il valori di default di 300DPI
            BufferedImage bufferedImage = renderer.renderImageWithDPI(
                    pageIdx,
                    lockOptions.getRenderImageDPI() != null ? lockOptions.getRenderImageDPI() : DEFAULT_DPI,
                    ImageType.RGB);

            // memorizzo l'immagine in formato JPG in uno stream in memoria
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", buffer);

            // aggiungo l'immagine nelle risorse del documento PDF renderizzato
            PDImageXObject imageXObject = PDImageXObject.createFromByteArray(renderedDocument, buffer.toByteArray(), null);

            // creo una nuova pagina, mantenendo le medesime dimensioni della pagina originale
            PDPage page = new PDPage(document.getPage(pageIdx).getMediaBox());

            // aggiungo la pagina nel documento renderizzato
            renderedDocument.addPage(page);

            // creo un content stream per la pagina attuale del docuemnto renderizzato
            PDPageContentStream contentStream = new PDPageContentStream(
                    renderedDocument,
                    page,
                    PDPageContentStream.AppendMode.APPEND,
                    true,
                    true);

            // applico sull'intera pagina l'immagine della pagina originale
            contentStream.drawImage(imageXObject, 0, 0, document.getPage(pageIdx).getMediaBox().getWidth(), document.getPage(pageIdx).getMediaBox().getHeight());

            // chiudo il content stream
            contentStream.close();
        }
        // chiudo il documento originale
        document.close();

        // ritorno il nuovo documento renderizzato
        return renderedDocument;
    }

}
