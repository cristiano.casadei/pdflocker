package it.maggioli.pdflocker.controller;

import it.maggioli.pdflocker.request.lockRequest.LockOptions;
import it.maggioli.pdflocker.request.lockRequest.WatermarkOptions;
import it.maggioli.pdflocker.service.PdfLockerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;


/**
 * Servizio di blocco dei file PDF
 *
 * @author CCasadei
 */
@RestController
@RequestMapping("/api/v1")
public class PdfLockerControllerV1 {

    private static final Logger logger = LoggerFactory.getLogger(PdfLockerControllerV1.class);

    private final PdfLockerService pdfLockerService;

    public PdfLockerControllerV1(PdfLockerService pdfLockerService) {
        this.pdfLockerService = pdfLockerService;
    }

    /**
     * Metodo di blocco dei file PDF
     *
     * @param file             File PDF da bloccare
     * @param watermarkOptions JSON (opzionale) contenente le opzioni per l'applicazione del watermark sulle pagine del PDF
     * @param lockOptions      JSON contenente le opzioni di blocco/renderizzazione del PDF
     * @return File PDF bloccato
     */
    @PostMapping(path = "lock", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public byte[] lock(
            @Valid @NotNull @RequestPart("file") MultipartFile file,
            @Valid @Null @RequestPart(value="watermark", required = false) WatermarkOptions watermarkOptions,
            @Valid @NotNull @RequestPart(value="options") LockOptions lockOptions) {

        logger.debug("-----------");
        logger.debug("Ricevuto file da proteggere");
        logger.debug("Nome: " + file.getName());
        logger.debug("Dimensioni: " + file.getSize());

        logger.debug("-----------");
        logger.debug("Opzioni di watermark");
        if (watermarkOptions != null) {
            logger.debug("Testo: " + (watermarkOptions.getText() != null ? watermarkOptions.getText() : "NULL"));
            logger.debug("Posizione: " + (watermarkOptions.getPosition() != null ? watermarkOptions.getPosition() : "NULL"));
        } else {
            logger.debug("NESSUNA OPZIONE FORNITA");
        }

        logger.debug("-----------");
        logger.debug("Opzioni di protezione");
        logger.debug("User Password: " + (lockOptions.getUserPassword() != null ? lockOptions.getUserPassword() : "NON FORNITA"));
        logger.debug("Owner Password: " + (lockOptions.getOwnerPassword() != null ? lockOptions.getOwnerPassword() : "NON FORNITA (verrà impostata casualmente)"));
        logger.debug("Can Merge: " + (lockOptions.getCanMerge() != null ? lockOptions.getCanMerge() : "NULL"));
        logger.debug("Can Copy: " + (lockOptions.getCanCopy() != null ? lockOptions.getCanCopy() : "NULL"));
        logger.debug("Can Print: " + (lockOptions.getCanPrint() != null ? lockOptions.getCanPrint() : "NULL"));
        logger.debug("Can Modify: " + (lockOptions.getCanModify() != null ? lockOptions.getCanModify() : "NULL"));
        logger.debug("Can Fill In: " + (lockOptions.getCanFillIn() != null ? lockOptions.getCanFillIn() : "NULL"));
        logger.debug("Can Screan Readers: " + (lockOptions.getCanScreenReader() != null ? lockOptions.getCanScreenReader() : "NULL"));
        logger.debug("Render Image: " + (lockOptions.getRenderImage() != null ? lockOptions.getRenderImage() : "NULL"));
        logger.debug("Render Image DPI: " + (lockOptions.getRenderImageDPI() != null ? lockOptions.getRenderImageDPI() : "NULL"));
        logger.debug("-----------");
        logger.debug("");

        return pdfLockerService.lock(file, watermarkOptions, lockOptions).toByteArray();
    }
}
