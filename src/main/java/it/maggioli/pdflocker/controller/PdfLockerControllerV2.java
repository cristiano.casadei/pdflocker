package it.maggioli.pdflocker.controller;

import it.maggioli.pdflocker.request.lockRequest.LockOptions;
import it.maggioli.pdflocker.request.lockRequest.WatermarkOptions;
import it.maggioli.pdflocker.service.PdfLockerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;


/**
 * Servizio di blocco dei file PDF
 *
 * @author CCasadei
 */
@RestController
@RequestMapping("/api/v2")
public class PdfLockerControllerV2 {

    private static final Logger logger = LoggerFactory.getLogger(PdfLockerControllerV2.class);

    private final PdfLockerService pdfLockerService;

    public PdfLockerControllerV2(PdfLockerService pdfLockerService) {
        this.pdfLockerService = pdfLockerService;
    }

    /**
     * Metodo di blocco dei file PDF
     *
     * @param file             File PDF da bloccare
     * @return File PDF bloccato
     */
    @PostMapping(path = "lock", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public byte[] lock(
            @Valid @NotNull @RequestPart("file") MultipartFile file,
            @Valid @Null @RequestParam(value = "watermarkText", required = false) String watermarkText,
            @Valid @Null @RequestParam(value = "watermarkPosition", required = false) String watermarkPosition,
            @Valid @Null @RequestParam(value = "optionUserPassword", required = false) String optionUserPassword,
            @Valid @Null @RequestParam(value = "optionOwnerPassword", required = false) String optionOwnerPassword,
            @Valid @Null @RequestParam(value = "optionCanMerge") Boolean optionCanMerge,
            @Valid @Null @RequestParam(value = "optionCanCopy") Boolean optionCanCopy,
            @Valid @Null @RequestParam(value = "optionCanPrint") Boolean optionCanPrint,
            @Valid @Null @RequestParam(value = "optionCanModify") Boolean optionCanModify,
            @Valid @Null @RequestParam(value = "optionCanFillIn") Boolean optionCanFillIn,
            @Valid @Null @RequestParam(value = "optionCanScreenReader") Boolean optionCanScreenReader,
            @Valid @Null @RequestParam(value = "optionRenderImage") Boolean optionRenderImage,
            @Valid @Null @RequestParam(value = "optionRenderImageDPI", required = false) Float optionRenderImageDPI
    ) {
        logger.debug("-----------");
        logger.debug("Ricevuto file da proteggere");
        logger.debug("Nome: " + file.getName());
        logger.debug("Dimensioni: " + file.getSize());

        WatermarkOptions watermarkOptions = null;
        if ((watermarkText != null) && (watermarkPosition != null)) {
            watermarkOptions = new WatermarkOptions();
            watermarkOptions.setText(watermarkText);
            watermarkOptions.setPosition(watermarkPosition);
        }
        logger.debug("-----------");
        logger.debug("Opzioni di watermark");
        if (watermarkOptions != null) {
            logger.debug("Testo: " + (watermarkOptions.getText() != null ? watermarkOptions.getText() : "NULL"));
            logger.debug("Posizione: " + (watermarkOptions.getPosition() != null ? watermarkOptions.getPosition() : "NULL"));
        } else {
            logger.debug("NESSUNA OPZIONE FORNITA");
        }

        LockOptions lockOptions = new LockOptions();
        lockOptions.setUserPassword(optionUserPassword);
        lockOptions.setOwnerPassword(optionOwnerPassword);
        lockOptions.setCanMerge(optionCanMerge);
        lockOptions.setCanCopy(optionCanCopy);
        lockOptions.setCanPrint(optionCanPrint);
        lockOptions.setCanModify(optionCanModify);
        lockOptions.setCanFillIn(optionCanFillIn);
        lockOptions.setCanScreenReader(optionCanScreenReader);
        lockOptions.setRenderImage(optionRenderImage);
        lockOptions.setRenderImageDPI(optionRenderImageDPI);
        logger.debug("-----------");
        logger.debug("Opzioni di protezione");
        logger.debug("User Password: " + (lockOptions.getUserPassword() != null ? lockOptions.getUserPassword() : "NON FORNITA"));
        logger.debug("Owner Password: " + (lockOptions.getOwnerPassword() != null ? lockOptions.getOwnerPassword() : "NON FORNITA (verrà impostata casualmente)"));
        logger.debug("Can Merge: " + (lockOptions.getCanMerge() != null ? lockOptions.getCanMerge() : "NULL"));
        logger.debug("Can Copy: " + (lockOptions.getCanCopy() != null ? lockOptions.getCanCopy() : "NULL"));
        logger.debug("Can Print: " + (lockOptions.getCanPrint() != null ? lockOptions.getCanPrint() : "NULL"));
        logger.debug("Can Modify: " + (lockOptions.getCanModify() != null ? lockOptions.getCanModify() : "NULL"));
        logger.debug("Can Fill In: " + (lockOptions.getCanFillIn() != null ? lockOptions.getCanFillIn() : "NULL"));
        logger.debug("Can Screan Readers: " + (lockOptions.getCanScreenReader() != null ? lockOptions.getCanScreenReader() : "NULL"));
        logger.debug("Render Image: " + (lockOptions.getRenderImage() != null ? lockOptions.getRenderImage() : "NULL"));
        logger.debug("Render Image DPI: " + (lockOptions.getRenderImageDPI() != null ? lockOptions.getRenderImageDPI() : "NULL"));
        logger.debug("-----------");
        logger.debug("");

        return pdfLockerService.lock(file, watermarkOptions, lockOptions).toByteArray();
    }
}
