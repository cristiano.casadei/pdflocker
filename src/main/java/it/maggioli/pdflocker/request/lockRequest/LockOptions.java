package it.maggioli.pdflocker.request.lockRequest;

import lombok.Getter;
import lombok.Setter;

/**
 * Modello dati per le opzioni di blocco dei file PDF
 *
 * @author CCasadei
 */
@Getter
@Setter
public class LockOptions {
    /**
     * Password utente di accesso al file PDF
     */
    private String userPassword;

    /**
     * Password owner per i permessi del file PDF.
     * Se non impostato, verrà utilizzata una password casuale
     */
    private String ownerPassword;

    /**
     * Abilita l'assemblaggio del documento PDF (merge con altri PDF, ad esempio)
     */
    private Boolean canMerge;

    /**
     * Abilita la copia del contenuto del PDF
     */
    private Boolean canCopy;

    /**
     * Abilita la stampa del PDF
     */
    private Boolean canPrint;

    /**
     * Abilita la modifica del PDF
     */
    private Boolean canModify;

    /**
     * Abilita la possibilità di editing dei campi form del PDF
     */
    private Boolean canFillIn;

    /**
     * Abilita la possibilità di utilizzare strumenti di accessibilità sul PDF
     */
    private Boolean canScreenReader;

    /**
     * Indica se il PDF deve essere renderizzato per renderlo grafico
     */
    private Boolean renderImage;

    /**
     * Risoluzione in DPI da utilizzare per la renderizzazione del PDF.
     * Di default sono 300DPI
     */
    private Float renderImageDPI;
}
