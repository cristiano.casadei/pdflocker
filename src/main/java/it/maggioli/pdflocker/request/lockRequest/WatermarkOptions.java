package it.maggioli.pdflocker.request.lockRequest;

import lombok.Getter;
import lombok.Setter;

/**
 * Modello dati per le opzioni del watermark da applicare alle pagine del PDF
 *
 * @author CCasadei
 */
@Getter
@Setter
public class WatermarkOptions {

    /**
     * Testo del waterark
     */
    private String text;

    /**
     * Posizione del watermark
     * Valori possibili (case-insensitive): top, bottom, left, right
     */
    private String position;
}