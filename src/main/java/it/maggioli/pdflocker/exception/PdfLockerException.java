package it.maggioli.pdflocker.exception;

/**
 * Classe di eccezzione specifica
 *
 * @author CCasadei
 */
public class PdfLockerException extends RuntimeException {
    public PdfLockerException(String message) {
        super(message);
    }

    public PdfLockerException(String message, Throwable cause) {
        super(message, cause);
    }
}
