package it.maggioli.pdflocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class PdfLockerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PdfLockerApplication.class, args);
    }

}
