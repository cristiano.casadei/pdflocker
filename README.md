# RESTful API per il blocco dei file PDF

**Autore: Cristiano Casadei**

### Maven + Docker Compose

Lanciare il comando `mvn compile war:war` per generare i war.

Copiarli nella sottocartella `deployments` in modo da poterli poi utilizzare con `docker-compose up --build` 

---

### Swagger UI url: `/swagger-ui/`

Questo è il percorso per ottenere la Swagger UI per i test 

**NOTA BENE IMPORTANTE!!** lo slash finale!

---

### API V1

[Clicca qui](V1.md)

---

### API V2

[Clicca qui](V2.md)